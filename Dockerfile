FROM registry.gitlab.com/therealxg/build_tools/arm64v8/ubuntu:21.04 AS prod
RUN	apt-get update && \
	 apt-get install -y --no-install-recommends sudo \
	 iproute2 && \
	 apt-get -y autoremove && \
	 apt-get -y clean
WORKDIR /root/
COPY message_router_tcp ./
COPY env_setup.sh ./
ENTRYPOINT ["/root/env_setup.sh"]
CMD ["sh", "-c", "./message_router_tcp $SERVERIP"]