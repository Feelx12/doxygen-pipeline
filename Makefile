EXENAME = message_router_tcp

OBJDIR = obj
# PIPELINE_COMPILER is full path from compiler used in pipeline. If not set (usually on CMD line) then it will just use the basic compiler name
CC = $(PIPELINE_COMPILER)aarch64-none-linux-gnu-gcc
RM = rm -rf
MKDIR = mkdir -p
SRC_ROOT = src

IFLAGS := -IInclude -Isrc -Iinclude/ -I/root/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/lib/include

MFLAGS :=

OFLAGS := -g
CFLAGS := -std=c99 $(OFLAGS) $(MFLAGS) $(IFLAGS) 

LDFLAGS = -static
LDLIBS = -lcurl -pthread

space := 
space += 
VPATH := $(subst $(space),:,$(shell find $(SRC_ROOT) -type d))

OMIT_SRCS := 

#S_SRCS  := $(filter-out $(OMIT_SRCS),$(shell find $(SRC_ROOT) -name "*.c"))
S_SRCS  := src/message_router_one.c
S_OBJS  := $(addprefix $(OBJDIR)/, $(notdir $(S_SRCS:.c=.o)))

all: $(EXENAME)

clean:
	$(RM) $(EXENAME) $(OBJDIR)

$(EXENAME): $(S_OBJS)
	@echo "Building $(EXENAME)..."
	$(CC) $(CFLAGS) -o $@ $(OBJDIR)/*.o $(LDFLAGS) $(LDLIBS)

$(OBJDIR)/%.o: %.c
	@$(MKDIR) $(OBJDIR)
	$(CC) -c $(CFLAGS) $< -o $@
