XGS Table of Contents
- [Summary](#summary)
- [MAVLink System ID and Component ID](#mavlink-system-id-and-component-id)
- [Ports](#ports)
- [Network Topology](#network-topology)
- [Compiling Locally](#compiling-locally)
## Summary

XGS will listen for client application TCP connections on port 14553 and handle messages to be sent to MavProxy.  A connection will also be made on port 14552 to communicate with MavProxy as well.  XGS will route mavlink messags sent to it by STOA and GeoFence to MavProxy, as well as route message back to STOA and GF from MavProxy.  STOA message will be sent without any interruptions as long as the drone is in the GeoFence, in the case that the drone breaches the GeoFence and a message is sent to XGS by GeoFence to indicate a breach, XGS will send a message to MavProxy to return to launch point and kill the docker container named "stoa".  XGS will handle dropped connections form client applications as well and they are able to reconnect to XGS in case of a drop connection. 

## MAVLink System ID and Component ID

| System      | SystemID:ComponentID |
| ----------- | -----------          |
| XGS         | 255:3                |
| GeoFence    | 255:2                |
| MissionPlane/STOA| 255:1                |
| Ardupilot   | 1:1                  |

## Ports
* Mission Applications send out on TCP Port 14553 and IP of Localhost
* XGS receives on TCP Port 14553 locally for mission apps and sends out on 14552 to Ardupilot with MAVProxy (or 5760 to Ardupilot without MAVProxy)

## Network Topology

```mermaid
graph LR
  subgraph TX2
    XGS[XGS]
    ST[STOA]
    GF[GeoFence]
    ST-->|TCP:14553|XGS
    GF-->|TCP:14553|XGS
    XGS-->|TCP:14553|ST
    XGS-->|TCP:14553|GF
  end 

  subgraph FC
    JSB[JSBSim]
    AP[ArduPilot]
    MP[MavProxy]
    AP-->|UDP:5502|JSB
    JSB-->|UDP:5501|AP
    AP-->|TCP:5760|MP
    MP-->|TCP:5760|AP
  end
  
  XGS-->|TCP:14552|MP
  MP-->|TCP:14552|XGS
  style TX2 fill:#93b7c4,stroke:#3a4952,stroke-width:2px
  style FC fill:#93b7c4,stroke:#3a4952,stroke-width:2px
  ```

## Compiling Locally
To easily cross-compile XGS for ARM, install the following tools:
* [VS Code](https://code.visualstudio.com/download)
* [VS Code "Remote - Containers" Extension](https://code.visualstudio.com/docs/remote/containers)
* [Docker](https://docs.docker.com/engine/install/ubuntu/)

Ensure you have the ability to pull Docker images from registry.gitlab.com/therealxg (Run:  `docker login registry.gitlab.com -u GITLABUSERNAME -p GITLAB_PAT` read [here](registry.gitlab.com/therealxg) to generate GITLAB_PAT).  
Clone this repository.  

1. Open VS Code
2. Press Ctrl+Shift+P
3. Type "Remote Open Folder" and select "Remote-Containers: Open Folder in Container..."
4. In the window that opens, select the XGS repository folder you just cloned.
   1. This will open a VS Code container envirnment and pull a [GCC ARM cross-compiler container](https://gitlab.com/therealxg/gcc_arm) from the Gitlab registry and place your code in that environment.
5. Once it has finished opening, select "Terminal" from the bottom window and type `make`. This will compile the program as an AARCH64 binary with required libraries.  