#!/bin/bash

#Check to see if an environment variable for server ip was sent in, if not
#set the server ip to the default gateway (hopefully the ip of the host machine) if multiple exist we will 
#have to change this to adapt
if [ -z "$SERVERIP" ]; then
  export SERVERIP=$(ip route | awk '/^default/ { print $3 }')
fi

#Start XGS with new variables
exec "$@"
