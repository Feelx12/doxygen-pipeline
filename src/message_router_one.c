/*******************************************************************************
XGS Router
 ****************************************************************************/
/*
 This router will recieve messages from client applications through TCP and will route
 then to Ardupilot.
 
 I compiled this program sucessfully on Ubuntu 18.04 with the following command
 
 gcc -I ../include -o message_router_one message_router_one.c
 
 the rt library is needed for the clock_gettime on linux
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
/* Linux / MacOS POSIX timer headers */
#include <sys/time.h>
#include <time.h>
#include <arpa/inet.h>
#include <stdbool.h> /* required for the definition of bool in C99 */

/* This assumes you have the mavlink headers in your include path
 or in the same folder as this source file */
#include <ardupilotmega/mavlink.h>
#include <containers.h>

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)
#define TRUE   1

void AddChildSocket (int max_clients, const int *client_socket, fd_set *readfds, int *max_sd);
void AcceptClientSocket(int socket, struct sockaddr_in locAddr, int addrlen, int max_clients, int *client_socket);
void ClientHandler (int socket, int max_clients, int *clients, struct sockaddr_in locAddr, int addrlen, fd_set readfds);
void MessageSocketHandler(int socket, int sd, int *clients, struct sockaddr_in locAddr, int addrlen);
void MessageToClient(int socket, int max_clients, const int *client_socket);
void SendToClients(int socket, int max_clients, const int *client_socket, mavlink_message_t msg);
void MessageRouter (int socket, mavlink_message_t msg);

int main(int argc, char* argv[])
{
	
	char help[] = "--help";
	char target_ip[9] = "127.0.0.1";
	
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int sock2 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int client_socket[5];
	int addrlen;
	int activity;
	int max_sd;
	int max_clients = 5;
	struct sockaddr_in gcAddr; 
	struct sockaddr_in locAddr;
	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;
	uint16_t len;
	int opt = TRUE;
	//set of socket descriptors
	fd_set readfds;
	
	// Check if --help flag was used
	if ((argc == 2) && (strcmp(argv[1], help) == 0))
    {
		printf("\n");
		printf("\tUsage:\n\n");
		printf("\t");
		printf("%s", argv[0]);
		printf(" <ip address of QGroundControl>\n");
		printf("\tDefault for localhost: tcp-server 127.0.0.1\n\n");
		exit(EXIT_FAILURE);
    }
	
	// Initialize client sockets to 0
	for (int i = 0; i < max_clients; i++)
	{
		client_socket[i] = 0;
	}

	// Set sock to allow multiple connections
	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	
	memset(&locAddr, 0, sizeof(locAddr));

	// Create the type of connection for locAddr
	locAddr.sin_family = AF_INET;
	locAddr.sin_addr.s_addr = INADDR_ANY;
	locAddr.sin_port = htons(14553); //Port to listen for mission applications incoming
	
	// Bind the socket to localhost port
	if (-1 == bind(sock,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)))
    {
		perror("error bind failed");
		close(sock);
		exit(EXIT_FAILURE);
    }

	// Maximum pending connection for socket
	if ((listen(sock, 5)) != 0)
	{
		printf("Listen failed...\n");
	}
	else
	{
		printf("Listening on %d\n", htons(locAddr.sin_port));
	}
	
	addrlen = sizeof(locAddr);
	
	memset(&gcAddr, 0, sizeof(gcAddr));

	// Create the type of connection for gcAddr
	gcAddr.sin_family = AF_INET;
	if (argc == 2)
	{
		gcAddr.sin_addr.s_addr = inet_addr(argv[1]);	
	}
	else
	{
		gcAddr.sin_addr.s_addr = inet_addr(target_ip);
	}
	gcAddr.sin_port = htons(14552); //Set to 5760 if not using mavproxy. Port to connect to GC or FC

	// Connect socket to ardupilot
	if (connect(sock2, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in)) < 0)
	{
		printf("Socket connection failed.\n");
	}
	printf ("Starting\n");
	
	while(TRUE)
	{
		// Clear master socket to set
		FD_ZERO(&readfds);

		// Add sockets to set
		FD_SET(sock, &readfds);
		FD_SET(sock2, &readfds);
		max_sd = sock2;

		//add child sockets to set
		AddChildSocket(max_clients, client_socket, &readfds, &max_sd);	

		//wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);

		if ((activity < 0) && (errno!=EINTR))  
        {  
            printf("select error");  
        }

		if (FD_ISSET(sock, &readfds))
		{
			AcceptClientSocket(sock, locAddr, addrlen, max_clients, client_socket);
		}
		
		ClientHandler(sock2, max_clients, client_socket, locAddr, addrlen, readfds);

		if (FD_ISSET(sock2, &readfds))
		{
			MessageToClient(sock2, max_clients, client_socket);
		
		}

	}
}

void AddChildSocket (int max_clients, const int *client_socket, fd_set *readfds, int *max_sd)
{
	int sd;
	for (int i = 0; i < max_clients; i++)
	{
		//socket descriptor
		sd = client_socket[i];

		//if valid socket descriptor then add to read list
		if(sd > 0)
			FD_SET(sd, readfds);

		//highest file descriptor number, need it for the select function
		if(sd > *max_sd)
			*max_sd = sd;
	}
}

void AcceptClientSocket(int socket, struct sockaddr_in locAddr, int addrlen, int max_clients, int *client_socket)
{
	int client_sock; 
	if ((client_sock = accept(socket, (struct sockaddr *)&locAddr, (socklen_t*)&addrlen)) < 0)
	{
		perror("accept");
		exit(EXIT_FAILURE);
	}
	//inform user of socket number - used in send and receive commands 
    printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , client_sock , inet_ntoa(locAddr.sin_addr) , ntohs (locAddr.sin_port)); 
	
	//add new socket to array of sockets 
    for (int i = 0; i < max_clients; i++)  
    {  
        //if position is empty 
        if( *client_socket == 0 )  
        {  
            *client_socket = client_sock;  
            printf("Adding to list of sockets as %d\n" , i);  
                 
            break;  
        } 
		client_socket++; 
    }

}

void ClientHandler (int socket, int max_clients, int *clients, struct sockaddr_in locAddr, int addrlen, fd_set readfds)
{
	int sd;
	
	for (int i = 0; i < max_clients; i++)
	{
		sd = *clients;
		if (FD_ISSET(sd, &readfds))
		{
			MessageSocketHandler(socket, sd, clients, locAddr, addrlen);
			
		}
		clients++;
	}
}

void MessageSocketHandler(int socket, int sd, int *clients, struct sockaddr_in locAddr, int addrlen)
{
	ssize_t recsize = 0;
	uint8_t buf[BUFFER_LENGTH];
	memset(buf, 0, BUFFER_LENGTH);

	recsize = recv(sd, (void *)buf, BUFFER_LENGTH, 0);
	if (recsize > 0)
	{
		// Something received - print out all bytes and parse packet
		mavlink_message_t msg;
		mavlink_status_t status;
		printf("Bytes Received: %ld\nDatagram: ", recsize);
		for (int i = 0; i < recsize; ++i)
		{
			if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
			{
				// Packet received
				printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
			}
		}
		printf("\n");
		MessageRouter(socket, msg);				
	}
	else
	{
		//Somebody disconnected , get his details and print 
	    getpeername(sd , (struct sockaddr*)&locAddr , (socklen_t*)&addrlen);  
	    printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(locAddr.sin_addr) , ntohs(locAddr.sin_port));  
	    //Close the socket and mark as 0 in list for reuse 
	    close( sd );  
	    *clients = 0; 
	}
}

void MessageToClient(int socket, int max_clients, const int *client_socket)
{
	mavlink_message_t msg;
	mavlink_status_t status;
	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;

	memset(buf, 0, BUFFER_LENGTH);
	
	recsize = recv(socket, (void *)buf, BUFFER_LENGTH, 0);
	for (int i = 0; i < recsize ; ++i)
	{
		if (mavlink_parse_char(MAVLINK_COMM_1, buf[i], &msg, &status))
		{
			// Packet received
			SendToClients(socket, max_clients, client_socket, msg);	
		}
	}
}

void SendToClients(int socket, int max_clients, const int *client_socket, mavlink_message_t msg)
{
	uint8_t buf2[BUFFER_LENGTH];
	uint16_t len;
	int sd2;

	memset(buf2, 0, BUFFER_LENGTH);

	len = mavlink_msg_to_send_buffer(buf2, &msg);
	for (int j = 0; j < max_clients; j++)
	{
		sd2 = client_socket[j];
		if (sd2 != socket && sd2 != -1)
		{
			send(sd2, buf2, len, 0);
		}
	}
}

void MessageRouter (int socket, mavlink_message_t msg)
{
	uint16_t len;
	ssize_t bytes_sent;
	uint8_t buf[BUFFER_LENGTH];
	char cont_str[]={"stoa"};
	int res_code = 0;

	if(msg.sysid == 255 && msg.compid == 1)
	{
		printf("This is missionplane!\n");
		memset(buf, 0, BUFFER_LENGTH);
		
		len = mavlink_msg_to_send_buffer(buf, &msg);
		bytes_sent = send(socket, buf, len, 0);
		printf("Bytes Sent: %ld\nDatagram: ", bytes_sent);
		printf("\n");
	}
	else if(msg.sysid == 255 && msg.compid == 2)
	{
		printf("This is GF!\n");
		memset(buf, 0, BUFFER_LENGTH);

		if(msg.msgid == 162)
		{
			// Drone out of bound of GF, send RTL message
			mavlink_msg_set_mode_pack(
			255, 	// @param system_id ID of this system
			2,	// @param component_id ID of this component (e.g. 200 for IMU)
			&msg, 			// @param msg The MAVLink message to compress the data into
			0, 		// @param target_system The system setting the mode
			1, 			// @param base_mode The new base mode (Should be 1 to use custom_mode)
			PLANE_MODE_RTL);			// @param custom_mode The new autopilot-specific mode. Defined in ArduCopter/defines.h
			len = mavlink_msg_to_send_buffer(buf, &msg);
			bytes_sent = send(socket, buf, len, 0);
			printf("Bytes Sent: %ld\nDatagram: ", bytes_sent);

			res_code = kill_container(cont_str);
			if (res_code < 0)
			{
				perror("kill container");
				exit(EXIT_FAILURE);
			}
		}
		else
		{
			len = mavlink_msg_to_send_buffer(buf, &msg);
			bytes_sent = send(socket, buf, len, 0);
			printf("Bytes Sent: %ld\nDatagram: ", bytes_sent);
		}
	}
	else
	{
		printf("System ID and Component ID Unknown, SYSID: %d COMPID: %d\n", msg.sysid, msg.compid);
	}
}